'use strict'
let changeBtn = document.querySelector('.change');

changeBtn.addEventListener('click', function () {
    if (document.documentElement.hasAttribute('theme')) {
        document.documentElement.removeAttribute('theme');
        localStorage.removeItem('theme');
    } else {
        document.documentElement.setAttribute('theme', 'color-theme');
        localStorage.setItem('theme', 'color-theme');
    }
})

window.onload = function () {
    if (localStorage.getItem('theme')==='color-theme'){
        document.documentElement.setAttribute('theme', 'color-theme');
    }
}